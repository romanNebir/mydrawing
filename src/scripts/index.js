"use strict";
const getElement = (selector) => document.querySelector(selector);
const canvas = getElement(".canvas");
canvas.width = window.innerWidth - 220;
canvas.height = window.innerHeight - 20;
const ctx = canvas.getContext("2d");
let coors = [];
// Clear canvas
getElement(".btn-clear").addEventListener("click", () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
});
// add new dot
getElement(".btn-add").addEventListener("click", () => {
    const x = getElement("#xDot");
    const y = getElement("#yDot");
    if (x.value && y.value) {
        ctx.fillRect(+x.value - 2, +y.value - 2, 4, 4);
        coors.push({ x: +x.value, y: +y.value });
        x.value = "";
        y.value = "";
    }
    ;
});
// cancel drawing
getElement(".btn-cancel").addEventListener("click", () => {
    coors.forEach((elem) => {
        ctx.clearRect(elem.x - 2, elem.y - 2, 4, 4);
    });
    coors = [];
});
// draw figure
getElement(".btn-draw").addEventListener("click", () => {
    coors.forEach((elem, index) => {
        if (index == 0) {
            ctx.beginPath();
            ctx.moveTo(elem.x, elem.y);
        }
        else if (index === coors.length - 1) {
            ctx.lineTo(elem.x, elem.y);
            ctx.lineTo(coors[0].x, coors[0].y);
            ctx.stroke();
        }
        else {
            ctx.lineTo(elem.x, elem.y);
        }
    });
    getElement(".perimeter").innerText = `${getPerimeter(coors)}`;
    getElement(".square").innerText = `${getSquare(coors)}`;
    coors = [];
});
// draw by clicking on canvas
canvas.addEventListener("click", (e) => {
    ctx.fillRect(e.offsetX - 2, e.offsetY - 2, 4, 4);
    coors.push({ x: e.offsetX, y: e.offsetY });
});
// get perimeter
const getPerimeter = (coors) => {
    let p = 0;
    if (coors.length > 1) {
        for (let i = 0; i < coors.length; i++) {
            if (i === coors.length - 1) {
                if (coors.length > 2) {
                    p += Math.sqrt(Math.pow((coors[0].x - coors[i].x), 2) + Math.pow((coors[0].y - coors[i].y), 2));
                }
            }
            else {
                p += Math.sqrt(Math.pow((coors[i + 1].x - coors[i].x), 2) +
                    Math.pow((coors[i + 1].y - coors[i].y), 2));
            }
        }
        p = +(p * 0.026458).toFixed(2);
    }
    return p;
};
// get square
function getSquare(coors) {
    let square = 0;
    if (coors.length > 2) {
        for (let i = 0; i < coors.length; i++) {
            if (i === coors.length - 1) {
                square += coors[i].x * coors[0].y;
                square -= coors[0].x * coors[i].y;
            }
            else {
                square += coors[i].x * coors[i + 1].y;
                square -= coors[i + 1].x * coors[i].y;
            }
        }
        square = +(0.5 * Math.abs(square) * Math.pow(0.026458, 2)).toFixed(2);
    }
    return square;
}
